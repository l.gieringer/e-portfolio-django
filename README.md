# ePortfolio Django 

This is an ePortfolio on the python webframework django.

## Table of contents
###### 1. Setup
###### 2. Tutorial
###### 3. Further information

## 1. Setup

With this ePortfolio I want to introduce you to Django, show how the MTC is handled here and generally speaking demonstrate how to rapidly set up your web application.

First of all, you need to have [Python](https://www.python.org/) installed on your operating system. A list of the supported Python versions can be found [here](https://docs.djangoproject.com/en/3.0/faq/install/#what-python-version-can-i-use-with-django) to select the version tailored to your demands. While installing, make sure you select "add python.exe to PATH" and install pip, since those will come in handy later on.
If your planning big or just can't live without a certain dabatase, you can have a look [here](https://docs.djangoproject.com/en/3.0/topics/install/#database-installation) how to set up everything you need to know. For our purposes, the pre-installed database [SQLite](https://www.sqlite.org/index.html) works just about fine.

If you plan to do things more seriously than just this tutorial, check out the tools [virtualenv](https://virtualenv.pypa.io/en/latest/) and [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/). They provide you isolated Python environments to easily separate your projects. They also allow installing packages without admin priviledges. Make sure which os as well as which command-line interpreter you're using. Here is how to install and create a virtual environment on Windows with command prompt:
```bash
py -m pip install virtualenv
py -m pip install virtualenvwrapper-win
mkvirtualenv NAME
#stop working on the environment
deactivate
#start again
workon NAME
```

Now we can finally install Django!
```bash
py -m pip install Django
```
If any problems occur or you need more info concerning your system, make sure to visit the [Django installation guide](https://docs.djangoproject.com/en/3.0/intro/install/).

## 2. Tutorial

The scope of this tutorial is to create a very simple todo site. It shows how to display all issues, create new tasks and delete the ones already finished.

To begin with, we need to create the project in our console and add an application to work on.
 ```bash
django-admin startproject Testproject
#To show everything works, let's start the server
python manage.py runserver
python manage.py startapp todo
```
Now we need to add the created app in the settings.py to the pre-installed ones in INSTALLED_APPS

To start coding, we need to build up our database. In Django every table is a class that subclasses [django.db.models.Model](https://docs.djangoproject.com/en/3.0/ref/models/instances/#django.db.models.Model) and each attribute of the model represents a database field. Now since we want a table **Todo** with a field **content**, we need to add the following code in our model.py and [migrate](https://docs.djangoproject.com/en/3.0/ref/django-admin/#django-admin-makemigrations) it in our project.
~~~python
# inside model.py

class Todo(models.Model):
    content = models.TextField()
~~~

To be able to show all tasks, add tasks and delete tasks, we need to access this database through three different functions. This view is basically the "controller" of our web server.
~~~python
# inside views.py

from django.http import HttpResponseRedirect
from .models import Todo


def TodoView(request):
    all_todo_items = Todo.objects.all()
    return render(request, 'todo.html', {'all_items': all_todo_items})


def AddTodo(request):
    new_item = Todo(content=request.POST['content'])
    if (new_item.content != ""):
        new_item.save()
    return HttpResponseRedirect('/todo/')


def DeleteTodo(request, todo_id):
    item_to_delete = Todo(id=todo_id)
    item_to_delete.delete()
    return HttpResponseRedirect('/todo/')
~~~

Those functions can be called when Django recognizes an url pattern, which has to be configured in the url.py. Leading slashes are not required and will trigger an warning.
As you can see in the path for DeleteTodo, we need to addd an integer to be able to access the specific object in our views and delete it.

The url file can be distributed inside every single application, but for demonstration purposes we use the simple version.

~~~python
# inside urls.py

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('todo/', TodoView),
    path('addTodo/', AddTodo),
    path('deleteTodo/<int:todo_id>/', DeleteTodo),
]
~~~

To complete the MTV structure, we need to add a template (which we already called in our *TodoView*()). Here we're going to keep it very simple and only add necessary components.
We need to create a new folder inside our testproject folder and must be defined in your settings.py. In TEMPLATES change the following point like so:
~~~python
# inside settings.py

TEMPLATES = [
    {
    ...
    'DIRS': [os.path.join(BASE_DIR, 'templates')],
    ...
    },
]

~~~
Inside this folder we need to create our todo.html and simply create a submit button with a textfield to add a new task, create a list to iterate through all items and show them. With every list item comes another submit button to delete it.
~~~html
<!-- inside todo.html -->

<form action="/addTodo/" method="post">{% csrf_token %}
    <input type="text" name="content"/>
    <input type="submit" value="Add"/>
</form>

<ul>
    {% for todo_item in all_items %}
    <li>{{ todo_item.content }}
        <form action="/deleteTodo/{{ todo_item.id }}/" method="post">{% csrf_token %}
            <input type="submit" value="Delete"/>
        </form>
    </li>
    {% endfor %}
</ul>
~~~

## 3. Further information

+ [Static folder to include scripts or stylesheets](https://docs.djangoproject.com/en/3.0/howto/static-files/)
+ [Handle other files](https://docs.djangoproject.com/en/3.0/topics/files/) for APIs
+ [Testing in Django (generally unittests)](https://docs.djangoproject.com/en/3.0/topics/testing/)
+ [Security in Django](https://docs.djangoproject.com/en/3.0/topics/security/)
+ [Optimization](https://docs.djangoproject.com/en/3.0/topics/performance/) (how to do things right)
+ or generally the [Django Documentation](https://docs.djangoproject.com/en/3.0/)
