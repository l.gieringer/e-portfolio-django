from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Todo


def TodoView(request):
    all_todo_items = Todo.objects.all()
    return render(request, 'todo.html', {'all_items': all_todo_items})


def AddTodo(request):
    new_item = Todo(content=request.POST['content'])
    if (new_item.content != ""):
        new_item.save()
    return HttpResponseRedirect('/todo/')


def DeleteTodo(request, todo_id):
    item_to_delete = Todo(id=todo_id)
    item_to_delete.delete()
    return HttpResponseRedirect('/todo/')
